#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '

# Мой конфиг
# Запуск Hyprland на alt+m
bind -x '"\em": Hyprland'
# Добавление локальной директории для исполняемых файлов
export PATH="${PATH}:${HOME}/.local/bin"
# XDG директории
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"