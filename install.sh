#!/bin/sh

installPath="/mnt/archinstallations"
scriptDir=$(dirname $(realpath "${0}"))
encryptedname_root="root"

devices=$((lsblk --paths --scsi --output name,model; lsblk --paths --nvme --output name,model) | grep --invert-match NAME)
options=()
while IFS= read -r line; do
  options+=("$line")
done <<< "${devices}"

PS3="select disk:"
select device_selected in "${options[@]}"; do
  disk=$(awk '{print $1}' <<< "${device_selected}")
  break
done

device1_default="${disk}"1
device2_default="${disk}"2
device3_default="${disk}"3
device1="${device1_default}"
device2="${device2_default}"
device3="${device3_default}"

read -r -p "Username: " username
read -s -r -p "User password: " user_password
echo ''
read -s -r -p "Root password: " root_password
echo ''
read -r -p "Hostname: " hostname
read -r -p "With encrypt (yes/no): " is_encrypt
if [ "${is_encrypt}" = "yes" ]; then
  read -s -r -p "Encrypt password: " encrypt_password
  echo ''
fi




chrootCmd()
{
  arch-chroot "${installPath}" sh -c "${1}"
}
timeSet()
{
  timedatectl set-timezone Europe/Moscow
}
diskUnmount()
{
  local mounts=$(mount | grep "${disk}" | awk '{print $3}' | sort -r)
  for mnt in $mounts; do
    umount "${mnt}"
  done
}
diskWipe()
{
  wipefs -af "${disk}"
}
diskPartitioning()
{
  (
    # create GPT table
    echo "g";

    # create efi partition
    echo "n";
    echo "1";
    echo "";
    echo "+512M";

    # set efi fs tag
    echo "t";
    echo "1";

    # create boot partition
    echo "n";
    echo "2";
    echo "";
    echo "+1G";

    # create root partition
    echo "n";
    echo "3";
    echo "";
    echo "";

    # write
    echo "w";
  ) | fdisk "${disk}"
}
encryptPart()
{
  if [ "${is_encrypt}" = "yes" ]; then
    echo "YES" | cryptsetup luksFormat "${device3_default}" --key-file <(echo -n "${encrypt_password}")
    cryptsetup open "${device3_default}" "${encryptedname_root}"  --key-file <(echo -n "${encrypt_password}")
    device3="/dev/mapper/${encryptedname_root}"
  fi
}
diskMkfs()
{
  mkfs.fat -F 32 "${device1}"
  mkfs.ext4 -F "${device2}"
  mkfs.btrfs --force "${device3}"
}
installDirMk()
{
  if [ ! -d "${installPath}" ]; then
    mkdir -p "${installPath}"
  fi
}
partRootBtrfsPrepare()
{
  mount "${device3}" "${installPath}"

  btrfs subvolume create "${installPath}/@"
  btrfs subvolume create "${installPath}/@home"
  btrfs subvolume create "${installPath}/@log"
  btrfs subvolume create "${installPath}/@backups"
  btrfs subvolume create "${installPath}/@snapshots"
  btrfs subvolume create "${installPath}/@docker"

  umount "${installPath}" -R
}
pacmanKeys()
{
  pacman -Sy --noconfirm archlinux-keyring
  pacman -Scc --noconfirm
}
pacmanOptimizations()
{
  sed -i 's/#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf

  local mutlilibLineNum=$(grep -n -m 1 '\[multilib\]' "/etc/pacman.conf" | cut -d: -f1)
  sed -i "${mutlilibLineNum} s/#//" "/etc/pacman.conf"

  local mutlilibLineNextNum=$(( mutlilibLineNum + 1 ))
  sed -i "${mutlilibLineNextNum} s/#//" "/etc/pacman.conf"

  pacman -Syy --noconfirm python3 reflector
  reflector --country Russia --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

  pacman -Rsn python3 reflector
  pacman -Scc --noconfirm
}
systemMount()
{
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@ "${device3}" "${installPath}"

  mkdir "${installPath}/home"
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@home "${device3}" "${installPath}/home"

  mkdir -p "${installPath}/var/log"
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@log "${device3}" "${installPath}/var/log"

  mkdir "${installPath}/backups"
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@backups "${device3}" "${installPath}/backups"

  mkdir "${installPath}/snapshots"
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@snapshots "${device3}" "${installPath}/snapshots"

  mkdir -p "${installPath}/var/lib/docker"
  mount -o rw,noatime,compress=lzo,ssd,ssd_spread,discard=async,space_cache=v2,subvol=/@docker "${device3}" "${installPath}/var/lib/docker"

  mkdir "${installPath}/boot"
  mount "${device2}" "${installPath}/boot"

  mkdir "${installPath}/boot/efi"
  mount "${device1}" "${installPath}/boot/efi"
}
systemInstall()
{
  local base=("linux-zen")
  base+=("linux-zen-headers")
  base+=("base")
  base+=("base-devel")
  base+=("btrfs-progs")
  base+=("bash-completion")
  base+=("intel-ucode")
  base+=("grub")
  base+=("dosfstools")
  base+=("easyeffects")
  base+=("efibootmgr")
  base+=("less")
  base+=("linux-firmware")
  base+=("networkmanager")
  base+=("git")
  base+=("openssh")
  base+=("btop")
  base+=("helix")
  base+=("wireguard-tools")

  local main=("code")
  main+=("discord")
  main+=("docker")
  main+=("docker-compose")
  main+=("docker-buildx")
  main+=("firefox")
  main+=("foot")
  main+=("gamemode")
  main+=("gamescope")
  main+=("grim")
  main+=("hyprland")
  main+=("keepassxc")
  main+=("otf-font-awesome")
  main+=("pavucontrol")
  main+=("pipewire")
  main+=("pipewire-pulse")
  main+=("qt5-base")
  main+=("qt5-wayland")
  main+=("slurp")
  main+=("swappy")
  main+=("steam")
  main+=("ttf-jetbrains-mono")
  main+=("waybar")
  main+=("wayland")
  main+=("wayland-protocols")
  main+=("wireplumber")
  main+=("wl-clipboard")
  main+=("wlroots")
  main+=("wofi")
  main+=("xdg-desktop-portal")
  main+=("xdg-desktop-portal-hyprland")
  main+=("xdg-desktop-portal-wlr")
  main+=("xdg-utils")

  local gpu_nvidia=("nvidia-dkms")
  gpu_nvidia+=("nvidia-settings")
  gpu_nvidia+=("nvidia-utils")
  gpu_nvidia+=("nvtop")
  gpu_nvidia+=("opencl-nvidia")
  gpu_nvidia+=("lib32-nvidia-utils")
  gpu_nvidia+=("acpilight")

  local gpu_intel=("mesa")
  gpu_intel+=("lib32-mesa")

  pacstrap -K "${installPath}" "${base[@]}" "${main[@]}" "${gpu_nvidia[@]}"
}

systemFstab()
{
  genfstab -U "${installPath}" > "${installPath}/etc/fstab"

  sed -i -E "s/subvolid=[0-9]+//g" "${installPath}/etc/fstab"
  sed -i "s/,,/,/g" "${installPath}/etc/fstab"
}
systemPacman()
{
  cp "/etc/pacman.d/mirrorlist" "${installPath}/etc/pacman.d/mirrorlist"

  sed -i 's/#ParallelDownloads/ParallelDownloads/' "${installPath}/etc/pacman.conf"

  local mutlilibLineNum=$(grep -n -m 1 '\[multilib\]' "${installPath}/etc/pacman.conf" | cut -d: -f1)
  sed -i "${mutlilibLineNum} s/#//" "${installPath}/etc/pacman.conf"

  local mutlilibLineNextNum=$(( mutlilibLineNum + 1 ))
  sed -i "${mutlilibLineNextNum} s/#//" "${installPath}/etc/pacman.conf"

  chrootCmd "pacman-key --init && pacman-key --populate archlinux && pacman -Syy"
}
systemTime()
{
  chrootCmd "ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime && hwclock --systohc"
}
systemLocale()
{
  echo "en_US.UTF-8 UTF-8" > "${installPath}/etc/locale.gen"
  echo "ru_RU.UTF-8 UTF-8" >> "${installPath}/etc/locale.gen"
  chrootCmd "locale-gen"

  echo "LANG=en_US.UTF-8" > "${installPath}/etc/locale.conf"

  echo "KEYMAP=ru" > "${installPath}/etc/vconsole.conf"
  echo "FONT=cyr-sun16" >> "${installPath}/etc/vconsole.conf"
}
systemHost()
{
  echo "${hostname}" > "${installPath}/etc/hostname"

  echo "127.0.0.1 localhost" > "${installPath}/etc/hosts"
  echo "::1       localhost" >> "${installPath}/etc/hosts"
  echo "127.0.1.1 ${hostname}" >> "${installPath}/etc/hosts"
}
systemUserRoot()
{
  chrootCmd "echo ${root_password} | passwd --stdin"
}
systemUser()
{
  chrootCmd "useradd -m -G video,docker,gamemode -s /bin/bash ${username}"
  chrootCmd "echo ${user_password} | passwd --stdin ${username}"

  cp -r -T "${scriptDir}/home" "${installPath}/home/${username}"
  cp -r -T "${scriptDir}/root" "${installPath}"

  echo "${username} ALL=(ALL:ALL) ALL" > "${installPath}/etc/sudoers.d/${username}"

  chrootCmd "chown -R ${username}:${username} /home/${username}"
}
systemMkinitcpio()
{
  local modules=""
  local binaries=""
  local files=""
  local hooks="base udev autodetect microcode modconf kms keyboard keymap"

  local confPath="${installPath}/etc/mkinitcpio.conf"

  local modules_intel="i915"

  if [ "${is_encrypt}" = "yes" ]; then
    hooks="${hooks} encrypt"
  fi

  hooks="${hooks} consolefont block filesystems fsck"

  echo "MODULES=(${modules})"   >  "${confPath}"
  echo "BINARIES=(${binaries})" >> "${confPath}"
  echo "FILES=(${files})"       >> "${confPath}"
  echo "HOOKS=(${hooks})"       >> "${confPath}"

  chrootCmd "mkinitcpio -P"
}
systemGrub()
{
  local default="0"
  local timeout="1"
  local cmd_line_default="loglevel=3"
  local cmd_line=""
  local preload_modules="part_gpt part_msdos"
  local timeout_style="menu"
  local terminal="console"
  local gfxmode="auto"
  local gfx_payload="keep"
  local disable_recovery="true"

  local confPath="${installPath}/etc/default/grub"

  echo "GRUB_DISTRIBUTOR=\"Arch\"" > "${confPath}"

  local cmd_line_nvidia="nvidia.modeset=1 nvidia_drm.fbdev=1"
  cmd_line_default="${cmd_line_default} ${cmd_line_nvidia}"

  # установить опции для загрузки зашифрованного раздела
  if [ "${is_encrypt}" = "yes" ]; then
    local uuid_regex="[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}"
    local device_encrypted_uuid=$(blkid | grep "${device3_default}" | grep -Po " UUID=\"${uuid_regex}\"" | grep -Po "${uuid_regex}")
    cmd_line="${cmd_line} cryptdevice=UUID=${device_encrypted_uuid}:${encryptedname_root} root=${device3}"
    echo "GRUB_ENABLE_CRYPTODISK=y" >> "${confPath}"
  fi

  echo "GRUB_DEFAULT=${default}"                            >> "${confPath}"
  echo "GRUB_TIMEOUT=${timeout}"                            >> "${confPath}"
  echo "GRUB_TIMEOUT_STYLE=${timeout_style}"                >> "${confPath}"
  echo "GRUB_CMDLINE_LINUX_DEFAULT=\"${cmd_line_default}\"" >> "${confPath}"
  echo "GRUB_CMDLINE_LINUX=\"${cmd_line}\""                 >> "${confPath}"
  echo "GRUB_PRELOAD_MODULES=\"${preload_modules}\""        >> "${confPath}"
  echo "GRUB_TERMINAL_INPUT=${terminal}"                    >> "${confPath}"
  echo "GRUB_GFXMODE=${gfxmode}"                            >> "${confPath}"
  echo "GRUB_GFXPAYLOAD_LINUX=${gfx_payload}"               >> "${confPath}"
  echo "GRUB_DISABLE_RECOVERY=${disable_recovery}"          >> "${confPath}"

  chrootCmd "grub-install --target=x86_64-efi --bootloader-id=${hostname} --recheck ${disk} && grub-mkconfig -o /boot/grub/grub.cfg"
}
systemNordicTheme()
{
  git clone --depth 1 https://github.com/EliverLara/Nordic.git "${installPath}/usr/share/themes/Nordic"
}
sysSystemd()
{
  chrootCmd "systemctl enable NetworkManager"
}
systemYay()
{
  git clone --depth 1 https://aur.archlinux.org/yay.git "${installPath}/home/${username}/.local/share/yay"
  echo "${username} ALL=(ALL) NOPASSWD: ALL" > "${installPath}/etc/sudoers.d/${username}_nopasswd"
  chrootCmd "chown -R ${username}:${username} /home/${username}/.local/share/yay && su - ${username} -c 'cd /home/${username}/.local/share/yay && makepkg --noconfirm -sirc && yay -Sy --noconfirm wob'"
  rm "${installPath}/etc/sudoers.d/${username}_nopasswd"
}
sysUserHomePermission()
{
  chrootCmd "chown -R ${username}:${username} /home/${username}"
}
unmountWork()
{
  umount -R "${installPath}"

  if [ "${is_encrypt}" = "yes" ]; then
    cryptsetup close "${encryptedname_root}"
  fi
}

timeSet
diskWipe
diskPartitioning
encryptPart
diskMkfs
installDirMk
partRootBtrfsPrepare
pacmanKeys
pacmanOptimizations
systemMount
systemInstall
systemFstab
systemPacman
systemTime
systemLocale
systemHost
systemUserRoot
systemUser
systemMkinitcpio
systemGrub
systemNordicTheme
sysSystemd
systemYay
sysUserHomePermission
unmountWork
